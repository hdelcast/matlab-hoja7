function [t,Y,ev,hchng_vec,err_vec]=mirkfehlb45(f,intv,y0,TOL,hmin,hmax,fac,facmax)

% evaluaciones
ev = 0;

% Butcher Tableau
a = [0,          0,          0,           0,          0,      0; ...
     1/4,        0,          0,           0,          0,      0; ...
     3/32,       9/32,       0,           0,          0,      0; ...
     1932/2197,  -7200/2197, 7296/2197,   0,          0,      0; ...
     439/216,    -8,         3680/513,    -845/4104,  0,      0; ...
     -8/27,      2,          -3544/2565,  1859/4104,  -11/40, 0];
c = [0, 1/4, 3/8, 12/13, 1, 1/2];
b1 = [25/216,0,1408/2565,2197/4104,-1/5,0];
b2 =[16/135, 0, 6656/12825, 28561/56430, -9/50, 2/55];

% t, Y 
m = length(c);
l = length(y0);
t = intv(1);
Y = y0;

% h ¿h inicial?
hchng_vec = [];
h = (hmax + hmin)/2;

% error_vec
err_vec = [];

% Bucle
n = 1;
while (t(n) < intv(end)) && (h >= hmin)

    % eval
    ev = ev + m;

    % Cálculo k
    k = zeros(l,m);
    for i=1:m
        k(:,i) = feval(f, t(n) + h * c(i), Y(:,n) + h * k * a(i,:)');
    end
    
    % Cálculo Error
    e1 = k * b1';
    %e2 = k * b2';
    e3 = k * (b2 - b1)';
    e4 = h * max(abs(e3));
    err_vec(:,n) = e4;
    

    % Tolerancia -> añadir valor
    if (e4 < h*TOL)
        Y(:,n+1) = Y(:,n) + h * e1; 
        t(n+1)= t(n) + h;
        hchng_vec(n+1) = h;
        n=n+1;
    end

    % Nuevo h
    if e4 == 0
        h = min([hmax, h * facmax]);
    else
        h = min([hmax, h * min([facmax, fac * ((h * TOL/e4))^(1/5)])]);
    end

end 

end