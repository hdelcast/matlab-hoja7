function [t,Y,ev,hchng_vec,err_vec]=midp45fsal(f,intv,y0,TOL,hmin,hmax,fac,facmax)
% evaluaciones
ev = 0;

% Butcher Tableau
a = [0, 0, 0, 0, 0, 0, 0;
    1/5, 0, 0, 0, 0, 0, 0;
    3/40, 9/40, 0, 0, 0, 0, 0;
    44/45, -56/15, 32/9, 0, 0, 0, 0;
    19372/6561, -25360/2187, 64448/6561, -212/729, 0, 0, 0;
    9017/3168, -355/33, 46732/5247, 49/176, -5103/18656, 0, 0;
    35/384, 0, 500/1113, 125/192, -2187/6784, 11/84, 0];
c = [0, 1/5, 3/10, 4/5, 8/9, 1, 1];
b1 = [35/384, 0, 500/1113, 125/192, -2187/6784, 11/84, 0];
b2 = [5179/57600, 0, 7571/16695, 393/640, -92097/339200, 187/2100, 1/40];

% t, Y 
m = length(c);
l = length(y0);
t = intv(1);
Y = y0;

% h ¿h inicial?
hchng_vec = [];
h = (hmax + hmin)/2;

% error_vec
err_vec = [];

% Bucle
n = 1;
while (t(n) < intv(end)) && (h >= hmin)

    % eval
    ev = ev + m;

    % Cálculo k
    k = zeros(l,m);
    for i=1:m
        k(:,i) = feval(f, t(n) + h * c(i), Y(:,n) + h * k * a(i,:)');
    end
    
    % Cálculo Error
    e1 = k * b1';
    %e2 = k * b2';
    e3 = k * (b2 - b1)';
    e4 = h * abs(e3);
    err_vec(n) = e4;
    

    % Tolerancia -> añadir valor
    if (e4 < h*TOL)
        Y(:,n+1) = Y(:,n) + h * e1; 
        t(n+1)= t(n) + h;
        hchng_vec(n+1) = h;
        n=n+1;
    end

    % Nuevo h
    if e4 == 0
        h = min([hmax, h * facmax]);
    else
        h = min([hmax, h * min([facmax, fac * ((h * TOL/e4))^(1/5)])]);
    end

end 

end