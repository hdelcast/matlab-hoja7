%% explosión
f = @(t,y) y^2;
f_exact = @(t) 1/(1-t);
intv = [0 2];
y0 = 1;
TOL = 0.01;
hmin = 10^(-5);
hmax = (intv(end) - intv(1))/50;
facmax = 5;
fac = 0.9;

%% Rígida
A = [-2 1; 998 -999];
B = @(t) [2*sin(t); 999*(cos(t)-sin(t))];
f = @(t, y) A*y + B(t);
f_exact = @(t) [2*exp(-t) + sin(t); 2*exp(-t) + cos(t)];
intv=[0 10];
y0=[2;3];
TOL = 0.01;
hmin = 10^(-5);
hmax = (intv(end) - intv(1))/50;
facmax = 5;
fac = 0.9;

%% mrkf45

[t,y,ev,hchng_vec,err_vec] = mirkfehlb45(f,intv,y0,TOL,hmin,hmax,fac,facmax);

plot(t,y)

%% extrapolación

[t,y,ev,hchng_vec,err_vec] = mieuler21(f,intv,y0,TOL,hmin,hmax,fac,facmax);

plot(t,y)

%% FSAL

[t,y,ev,hchng_vec,err_vec] = mieuler12fsal(f,intv,y0,TOL,hmin,hmax,fac,facmax);

plot(t,y)

%% Dorman-Prince

[t,y,ev,hchng_vec,err_vec] = midp45fsal(f,intv,y0,TOL,hmin,hmax,fac,facmax);

plot(t,y)

