function [t,Y,ev,hchng_vec,err_vec]=mieuler12fsal(f,intv,y0,TOL,hmin,hmax,fac,facmax)

% evaluaciones
ev = 0;

% Butcher Tableau
a = [0,     0; ...
     1,     0];
c = [0, 1];
b1 = [  1,   0];
b2 = [1/2, 1/2];

% t, Y
m = length(c);
l = length(y0);
t = intv(1);
Y = y0;

% h
hchng_vec = [];
h = (hmax + hmin)/2;

% error_vec
err_vec = [];

% Bucle
n = 1;
while (t(n) < intv(end)) && (h >= hmin)

    % eval
    ev = ev + m;

    % Cálculo k
    k = zeros(l,m);
    for i=1:m
        k(:,i) = feval(f, t(n) + h * c(i), Y(:,n) + h * k * a(i,:)');
    end

    % Métodos
    e1 = k * b1';
    %e2 = k * b2';
    
    % Cálculo Error
    e3 = k * (b2 - b1)';
    e4 = h * abs(e3);
    err_vec(n) = e4;
    
    % Tolerancia -> añadir valor
    if (e4 < h*TOL)
        Y(:,n+1) = Y(:,n) + h * e1; 
        t(n+1)= t(n) + h;
        hchng_vec(n+1) = h;
        n=n+1;
    end

    % Nuevo h
    if e4 == 0
        h = min([hmax, h * facmax]);
    else
        h = min([hmax, h * min([facmax, fac * ((h * TOL/e4))^(1/5)])]);
    end

end 

end